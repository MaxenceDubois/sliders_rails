json.extract! slide, :id, :name, :text_l, :text_r, :created_at, :updated_at
json.url slide_url(slide, format: :json)